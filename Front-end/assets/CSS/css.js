import { StyleSheet } from "react-native";

const css = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    textPage: {
        backgroundColor: 'orange',
        padding: 20
    },
    containerTop: {
        justifyContent: 'flex-start'
    },
    container2: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    container3: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#fff',
        padding: 5,
    },
    container4: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#fff',
        padding: 5,
    },
    container5: {
        flex: 0,
        flexDirection: 'column',
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 5,
    },
    button__home: {
        margin: 20,
        alignItems: 'center',

    },
    darkbg: {
        backgroundColor: "#333"
    },
    login__logomarca: {
        marginBottom: 10
    },
    login__msg: (text = 'none') => ({
        fontWeight: "bold",
        fontSize: 22,
        color: '#fff2',
        marginBottom: 15,
        display: text
    }),
    login__form: {
        width: "80%"
    },
    login__input: {
        backgroundColor: "#fff",
        fontSize: 19,
        padding: 7,
        marginBottom: 15
    },
    login__button: {
        padding: 12,
        backgroundColor: "#F58634",
        alignSelf: "center",
        borderRadius: 5,

    },
    menu__button_Editar: {
        padding: 12,
        marginBottom: 10,
        backgroundColor: "#888",
        alignSelf: "auto",
        borderRadius: 5,

    },
    menu__button_Excluir: {
        padding: 12,
        paddingLeft: 10,
        marginBottom: 10,
        backgroundColor: "#333",
        alignSelf: "auto",
        borderRadius: 5,

    },
    menu__buttonText: {
        margin: 1,
        alignSelf: "center",
        fontWeight: "bold",
        fontSize: 10,
        color: "#fff",
    },
    login__buttonText: {
        fontWeight: "bold",
        fontSize: 22,
        color: "#333"
    },
    area__tab: {
        backgroundColor: '#333',
        fontSize: 20,
        fontWeight: 'bold',
        color: '#333'
    },
    area__menu: {
        flexDirection: 'row',
        marginTop: 20,
        paddingTop: 15,
        paddingBottom: 15,
        width: '100%',
        backgroundColor: '#111',
        alignItems: 'center',
        justifyContent: 'center'
    },
    area__menu2: {
        flexDirection: 'row',
        paddingTop: 10,
        paddingBottom: 10,
        width: '100%',
        backgroundColor: '#111',
        alignItems: 'center',
        justifyContent: 'center'
    },
    area__menu3: {
        flexDirection: 'row',
        paddingTop: 10,
        paddingBottom: 10,
        width: '33%',
        backgroundColor: '#111',
        alignItems: 'center',
        justifyContent: 'center'
    },
    area__menu4: {
        flexDirection: 'row',
        paddingTop: 5,
        paddingBottom: 5,
        backgroundColor: '#555',
        alignItems: 'center',
        justifyContent: 'center'
    },
    area__menu5: {
        paddingTop: 5,
        paddingBottom: 5,
        width: '80%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    button__home2: {
        textAlign: 'left',
    },
    area__title: {
        width: '80%',
        fontWeight: 'bold',
        fontSize: 20,
        color: '#fff',
        textAlign: 'center'
    },
    area__title2: {
        width: '80%',
        fontWeight: 'bold',
        fontSize: 20,
        color: '#000',
        textAlign: 'left'
    },
    area__menu6: {
        flexDirection: 'row',
        width: '100%',
        paddingTop: 5,
        paddingBottom: 5,
    },
    button__logout: {
        textAlign: 'right'
    },
    menuViajens: {
        flex: 1,
        justifyContent: "space-between",
        flexDirection: "column"
    },
    text_perfil: {
        padding: 10,
        fontWeight: 'bold',
        fontSize: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#000'

    },
    text_perfil2: {
        padding: 10,
        color: '#fff2',
        fontWeight: 'bold',
        fontSize: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#000',
    },
    footer: {
        paddingHorizontal: 20
    },
    footerItem: {
        paddingHorizontal: 10,
        paddingVertical: 10,
        backgroundColor: '#fff2',
        borderRadius: 8,
        marginTop: 7,
        borderColor: '#ccc',
        borderWidth: 1
    },
    footerTxt: {
        color: 'blue',
        fontSize: 16,
        textAlign: 'center'
    },
    footerTxtDelete: {
        color: 'red',
        fontSize: 16,
        fontWeight: '600',
        textAlign: 'center'
    },
    overlay: {
        backgroundColor: 'rgba(0,0,0,0.5)',
        position: 'absolute',
        top: 0,
        left: 0
    },
    popup: {
        borderRadius: 8,
        borderColor: '#333',
        borderWidth: 1,
        backgroundColor: '#333',
        paddingHorizontal: 10,
        paddingVertical: 10,
        position: 'absolute',
        top: 100,
        right: 15,
        left: 15,
    },
    select: {
        width: 200,
        height: 50,
    },
    dateComponente: {
        width: 350
    },

});

export { css };