import React from "react";
import Icon from 'react-native-vector-icons/FontAwesome';
import { Text, View, Button, Alert, TouchableOpacity } from 'react-native';
import { css } from '../../assets/CSS/css';
import AsyncStorage from "@react-native-async-storage/async-storage";

export default function MenuSistema(props) {

    async function logout() {
        await AsyncStorage.clear();
        props.navigation.navigate('Login');
    }

    return (
        <View style={css.area__menu}>
            <TouchableOpacity style={css.button__home2} onPress={() => props.navigation.navigate('Menu')}>
                <Icon name="home" size={20} color="#999" />
            </TouchableOpacity>
            <Text style={css.area__title}>{props.title}</Text>
            <TouchableOpacity style={css.button__logout} onPress={() => logout()}>
                <Icon name="sign-out" size={20} color="#999" />
            </TouchableOpacity>
        </View>
    );
}