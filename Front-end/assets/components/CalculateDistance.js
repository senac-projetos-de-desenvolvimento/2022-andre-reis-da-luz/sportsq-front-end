import axios from 'axios';

const GOOGLE_API_KEY = 'AIzaSyAIZfbfDC-OhMEOGocwHsx66e8nA4q6RF0';

const calculateDistanceAndTime = async (origin, destination) => {
  try {
    const url = `https://maps.googleapis.com/maps/api/directions/json?origin=${origin}&destination=${destination}&key=${GOOGLE_API_KEY}`;

    const response = await axios.get(url);

    if (response.data.status === 'OK') {
      const distance = response.data.routes[0].legs[0].distance.text;
      const duration = response.data.routes[0].legs[0].duration.text;

      return { distance, duration };
    } else {
      throw new Error('Não foi possível obter a distância e o tempo estimado.');
    }
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export default calculateDistanceAndTime;