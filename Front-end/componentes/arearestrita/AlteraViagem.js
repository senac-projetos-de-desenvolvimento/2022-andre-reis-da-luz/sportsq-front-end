import axios from 'axios';
import React, { useState, useEffect, useRef } from 'react';
import {
     Text, View, KeyboardAvoidingView, TouchableOpacity, TextInput, ScrollView, Platform, StyleSheet, Easing, Button, Modal, True, SafeAreaView, Pressable, Animated, Alert
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { css } from '../../assets/CSS/css';
import AsyncStorage from "@react-native-async-storage/async-storage";
import MenuSistema from '../../assets/components/MenuSistema';
import { Picker } from '@react-native-picker/picker';

import DateTimePicker from '@react-native-community/datetimepicker';

import { Autocomplete } from '@react-google-maps/api'
import calculateDistanceAndTime from '../../assets/components/CalculateDistance';



//url = "https://transport-calculator-back-dev-719001b96509.herokuapp.com"
url = "http://192.168.18.23:3000"

export default function AlteraViagem({ navigation, route }) {

     const [idViagem, setIdViagem] = useState(route.params.selectedViagem)



     const [origin, setOrigin] = useState('');
     const [destination, setDestination] = useState('');
     const [distance, setDistance] = useState('');
     const [duration, setDuration] = useState('');

     const [display, setDisplay] = useState('none');
     const [erroText, setErroText] = useState('')

     const [cliente, setCliente] = useState('');
     const [telefone, setTelefone] = useState('');
     const [placa, setPlaca] = useState('');
     const [carga, setCarga] = useState('');
     const [valorSugerido, setValorSugerido] = useState('');
     const [distanciaKm, setDistanciaKm] = useState(null);
     const [duracaoViagem, setDuracaoViagem] = useState(null);
     const [media, setMedia] = useState('')

     const [veiculoID, setVeiculoID] = useState('')
     const [custo, setCusto] = useState('')
     const [cobrado, setCobrado] = useState('')
     const [cobradoAntigo, setCobradoAntigo] = useState('')


     const [precoCombustivel, setPrecoCombustivel] = useState('')
     const [dados, setDados] = useState([])
     const [user, setUser] = useState(' ')
     const [userID, setUserID] = useState(' ')

     const [options, setOptions] = useState([]);
     const [selectedValue, setSelectedValue] = useState('');

     const [date, setDate] = useState(Date())
     const [show, setShow] = useState(false);

     ///////////////////////////////////////////////////////////////

     const createAlertCadastro = () =>
          Alert.alert(
               "Aviso:",
               "Sucesso! Viagem Atualizada.", [{
                    text: "Ok",
                    onPress: () => navigation.navigate('Menu')
               }]
          );

     const createAlertVerificaEncerrar = () =>
          Alert.alert(
               "Aviso:",
               "Atenção! Deseja Encerrar a Viagem?", [{
                    text: 'Cancelar',
                    style: 'cancel',
                },
               {
                    text: "Confirmar",
                    onPress: () => encerraViagem()
               }]
          );

     const createAlertEncerrar = () =>
          Alert.alert(
               "Aviso:",
               "Sucesso! Viagem Encerrada.", 
               [{
                    text: "Ok",
                    onPress: () => navigation.navigate('Menu')
               }]
          );



     ///////////////////////////////////////////////////////////////

     const onChange = (event, selectedDate) => {
          const currentDate = selectedDate || date;
          setShow(false);
          setDate(currentDate);
     };

     const showDatePicker = () => {
          setShow(true);
     };

     ///////////////////////////////////////////////////////////////

     async function chamaDados() {
          console.log(`Recebe ID da Viagem por Params: ${route.params.selectedViagem}`)
          console.log(`ID da Viagem: ${idViagem}`)
          console.log(`Solicitação sobre dados da viagem: ${url}/viagens/${idViagem}`)
          const responseGet = await axios.get(`${url}/viagens/${idViagem}`)
          const recebeDados = responseGet.data
          const identificaViagem = recebeDados[0]
          console.log(identificaViagem.data)

          console.log("Endereço Inicial: ", identificaViagem.enderecoInicial)
          setOrigin(identificaViagem.enderecoInicial)

          console.log("Endereço Final: ", identificaViagem.enderecoFinal)
          setDestination(identificaViagem.enderecoFinal)

          console.log("Distancia: ", identificaViagem.distanciaKm)
          setDistance(identificaViagem.distanciaKm)

          console.log("Duração: ", identificaViagem.duracaoHoras)
          setDuration(identificaViagem.duracaoHoras)

          console.log("Cliente: ", identificaViagem.cliente)
          setCliente(identificaViagem.cliente)

          console.log("Telefone: ", identificaViagem.telefone)
          setTelefone(identificaViagem.telefone)

          console.log("Placa: ", identificaViagem.placa)
          setPlaca(identificaViagem.placa)

          console.log("Carga: ", identificaViagem.carga)
          setCarga(identificaViagem.carga)

          console.log("Valor Cobrado: ", identificaViagem.valorCobrado)
          setCobradoAntigo(identificaViagem.valorCobrado)

          console.log("Custo: ", identificaViagem.custo)
          setCusto(identificaViagem.custo)

          console.log("Distancia: ", identificaViagem.distanciaKm)
          setDistanciaKm(identificaViagem.distanciaKm)

          console.log("Data: ", identificaViagem.dataDaViagem)
          setDate(identificaViagem.dataDaViagem)

          ///////////////////////////////////////////////////////////////

          console.log(`Data Armazenada: ${date}`)
          const value = await AsyncStorage.getItem("@asyncUsername")
          console.log(`Dados recolhidos do AsyncStorage ${value}`)
          setUser(value)

          console.log((`${url}/usuarios/valida/${value}`))
          const response = await axios.get(`${url}/usuarios/valida/${value}`)
          const dados2 = response.data

          console.log(`ID retirado do GET: ${dados2.id}`)
          const loginID = dados2.id
          setDados(loginID)

          setUserID(dados2.id)
          console.log(`ID do Usuário: ${loginID}`)

          console.log(`Valor do Combustível retirado do GET: ${dados2.precoCombustivel}`)
          const loginCombustivel = dados2.precoCombustivel
          setPrecoCombustivel(loginCombustivel)

          ///////////////////////////////////////////////////////////////
          const responseGetPlacas = await axios.get(`${url}/veiculos/retornarplacas/${loginID}`)
          const identificaPlaca = responseGetPlacas.data
          setOptions(identificaPlaca)
          console.log(`Dados retirados do GET-Veículos: ${responseGetPlacas.data}`)
          console.log(`Veículos do cliente: ${identificaPlaca}`)
     }

     const handleCalculate = async () => {
          try {

               const response2 = await axios.get(`${url}/veiculos/${placa}`)
               console.log(`Recebido da verificação de Placa: ${response2.data}`)
               console.log(`Recebido da verificação de Placa: ${response2.id}`)
               resp = response2.data
               for (i = 0; i <= (resp.length); i++) {
                    if (placa === (resp[i].placa)) {
                         setVeiculoID(resp[i].id)
                         console.log(`ID do Veículo: ${veiculoID}`)
                    }
               }

               const response3 = await axios.get(`${url}/media/${placa}`)
               console.log(`Recebido pela Media: ${response3.data}`)
               setMedia(response3.data)
               console.log(`Media do Veículo recebido pelo get: ${media}`)


               console.log(`Recebido pelo get: ${origin}`)
               console.log(`Recebido pelo get: ${destination}`)
               const result = await calculateDistanceAndTime(origin, destination);
               setDistance(result.distance);
               setDuration(result.duration);
               console.log(`Aqui valor antes do slice ${distance}`)
               const custoViagem = Number((distance.slice(0, 3)))
               console.log(`Distancia da Viagem: ${custoViagem}`)
               setDistanciaKm(custoViagem)


               console.log(`Aqui valor duração antes do slice ${duration}`)
               if (duration.length > 7) {
                    const horas = Number(duration.slice(0, 2))
                    console.log(`Horas: ${horas}`)
                    const horasParaM = horas * 60
                    console.log(`calc: ${horasParaM}`)
                    const minutos = Number(duration.slice(8, 10))
                    console.log(`Minutos: ${minutos}`)
                    const duracaoViagem = horasParaM + minutos
                    console.log(`Duração da Viagem(Min): ${duracaoViagem}`)
                    const duracaoViagemH = duracaoViagem / 60
                    console.log(`Duração da Viagem(Hr): ${duracaoViagemH}`)
                    setDuracaoViagem(duracaoViagemH)
               } else {
                    const minutos = (duration.slice(0, 2))
                    const duracaoViagem = Number(minutos)
                    console.log(`Duração da Viagem(Minutos): ${duracaoViagem}`)
                    setDuracaoViagem(duracaoViagem)
               }

               const distanciaNumber = Number(custoViagem).toFixed(2)
               const mediaNumber = Number(media).toFixed(2)
               const gastoLitros = (distanciaNumber / mediaNumber).toFixed(2)
               const litro = precoCombustivel
               const gasto = gastoLitros * litro


               console.log(`Media Recebida: ${mediaNumber}`)
               console.log(`Distancia Recebida: ${distanciaNumber}`)
               console.log(`Media do Veículo: ${media}`)
               console.log(`Gasto do Veículo/L: ${gastoLitros}`)
               console.log(`Valor do L: ${litro}`)
               console.log(`Custo da Viagem: ${gasto}`)
               setCusto(gasto)

               if (gasto < 2) {
                    const valorFinal = ((gasto * 2) + 6).toFixed(2)
                    console.log(`Valor a ser Cobrado: ${valorFinal}`)

                    setValorSugerido(valorFinal)
               }
               if ((gasto > 2) && (gasto <= 6)) {
                    const valorFinal = ((gasto * 1.2) + 7).toFixed(2)
                    console.log(`Valor a ser Cobrado: ${valorFinal}`)

                    setValorSugerido(valorFinal)
               }

               if (gasto > 6 && gasto < 50) {
                    const valorFinal = ((gasto * 1.5) + 4).toFixed(2)
                    console.log(`Valor a ser Cobrado: ${valorFinal}`)

                    setValorSugerido(valorFinal)
               }
               if (gasto > 50) {
                    const valorFinal = ((gasto * 1.5)).toFixed(2)
                    console.log(`Valor a ser Cobrado: ${valorFinal}`)

                    setValorSugerido(valorFinal)
               }

          } catch (error) {
               setErroText('Erro ao realizar cálculos. Tente novamente mais tarde.')
               console.log(error);
          }
     };
     async function sendForm() {


          let response = await fetch(`${url}/viagens/${idViagem}`, {
               method: 'PUT',
               headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
               },
               body: JSON.stringify({
                    cliente: cliente,
                    motorista: user,
                    telefone: telefone,
                    placa: selectedValue,
                    carga: carga,
                    enderecoInicial: origin,
                    enderecoFinal: destination,
                    valorCobrado: cobrado,
                    custo: custo,
                    duracaoHoras: duracaoViagem,
                    usuario_id: userID,
                    distanciaKm: distanciaKm,
                    veiculo_id: veiculoID,
                    dataDaViagem: date,
                    emAndamento: true
               })
          });
          let json = await response.json();
          console.log(json)
          console.log(json.id)
          if (json.id === 0) {
               setErroText(json.msg)
               setDisplay('flex');
               setTimeout(() => {
                    setDisplay('none');
               }, 5000);
          } else {
               createAlertCadastro()

          }
     }

     useEffect(() => {
          chamaDados()

     }, [])


     async function encerraViagem() {
          console.log(`Viagem será encerrada.`)
          console.log(`ID da Viagem: ${idViagem}`)

          let response = await fetch(`${url}/viagens/${idViagem}`, {
               method: 'PUT',
               headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
               },
               body: JSON.stringify({
                    cliente: cliente,
                    emAndamento: false
               })
          });
          let json = await response.json();
          console.log(json)
          console.log(json.id)
          if (json.id === 0) {
               setErroText(json.msg)
               setDisplay('flex');
               setTimeout(() => {
                    setDisplay('none');
               }, 5000);
          } else {
               createAlertEncerrar()

          }
     }

     const [visible, setVisible] = useState(false)
     const scale = useRef(new Animated.Value(0)).current;
     function resizeBox(to) {
          to === 1 && setVisible(true);
          Animated.timing(scale, {
               toValue: to,
               useNativeDriver: true,
               duration: 200,
               easing: Easing.linear,
          }).start(() => to === 0 && setVisible(false));
     }

     return (

          <View style={[css.container, css.containerTop]}>

               <MenuSistema title='Informações da Viagem' navigation={navigation} />
               <View style={{
                    paddingTop: 2,
                    paddingBottom: 2,
                    justifyContent: 'center'
               }}>
                    <SafeAreaView style={{
                         flexDirection: 'row',
                         paddingTop: 5,
                         paddingBottom: 2,
                    }}>
                         <View style={{ width: '80%', }}></View>
                         <View >
                              <TouchableOpacity style={css.menu__button_Editar} onPress={() => createAlertVerificaEncerrar()}>
                                   <Icon name="check" size={15} color="#999" backgroundColor='#777' />
                              </TouchableOpacity>
                         </View>
                    </SafeAreaView>
                    <KeyboardAvoidingView
                         behavior={Platform.OS == "ios" ? "padding" : "height"} style={[css.container]}>
                         <ScrollView>
                              <View style={css.container4}>
                                   <View style={css.area__menu6}>
                                        <View style={{
                                             width: '50%'
                                        }}>
                                             <Text style={css.text_perfil}>
                                                  Cliente:
                                             </Text>
                                             <TextInput
                                                  placeholder={cliente}
                                                  value={cliente}
                                                  onChangeText={setCliente}
                                                  style={css.text_perfil}
                                             />
                                        </View>
                                        <View style={{
                                             width: '50%'
                                        }}>
                                             <Text style={css.text_perfil}>
                                                  Telefone:
                                             </Text>
                                             <TextInput
                                                  placeholder={telefone}
                                                  value={telefone}
                                                  onChangeText={setTelefone}
                                                  style={css.text_perfil}
                                             />
                                        </View>

                                   </View>
                                   <Text></Text>
                                   <View style={css.area__menu6}>
                                        <View style={{
                                             width: '50%'
                                        }}>

                                             <Picker
                                                  prompt={placa}
                                                  style={css.text_perfil}
                                                  selectedValue={selectedValue}
                                                  onValueChange={(itemValue, indexItem) =>
                                                       setSelectedValue(itemValue)
                                                  }>
                                                  {options.map(option => {
                                                       return <Picker.Item
                                                            style={css.text_perfil}
                                                            key={option}
                                                            label={option}
                                                            value={option}
                                                       />
                                                  }
                                                  )}
                                             </Picker>
                                        </View>
                                        <View style={{
                                             width: '50%'
                                        }}>
                                             <Text style={css.text_perfil}>
                                                  Carga:
                                             </Text>
                                             <TextInput
                                                  placeholder={carga}
                                                  value={carga}
                                                  onChangeText={setCarga}
                                                  style={css.text_perfil}
                                             />
                                        </View>
                                   </View>
                                   <Text></Text>
                                   <View style={css.area__menu6}>

                                        <TouchableOpacity onPress={showDatePicker}>
                                             <Text style={css.text_perfil}>
                                                  Data: {date}
                                             </Text>
                                        </TouchableOpacity>
                                        {show && (
                                             <DateTimePicker
                                                  value={date}
                                                  mode="date"
                                                  display="default"
                                                  onChange={onChange}
                                             />
                                        )}

                                   </View>
                                   <Text></Text>
                                   <View style={css.area__menu6}>
                                        <View style={{
                                             width: '50%'
                                        }}>
                                             <Text style={css.text_perfil}>
                                                  Endereço Inicial:
                                             </Text>
                                             <TextInput
                                                  placeholder={origin}
                                                  //Avenida Pinheiro Machado, 927 - Pelotas / RS

                                                  value={origin}
                                                  onChangeText={setOrigin}
                                                  style={css.text_perfil}
                                             />
                                        </View>
                                        <View style={{
                                             width: '50%'
                                        }}>
                                             <Text style={css.text_perfil}>
                                                  Endereço Final:
                                             </Text>
                                             <TextInput
                                                  placeholder={destination}
                                                  //Rua Antonio Joaquim Dias, 91 - Pelotas / RS //// R. Gonçalves Chaves, 605 - Centro, Pelotas - RS, 96015-560
                                                  value={destination}
                                                  onChangeText={setDestination}
                                                  style={css.text_perfil}
                                             />
                                        </View>

                                   </View>
                                   <Text></Text>
                                   <View style={css.area__menu6}>
                                        <View style={{
                                             width: '50%'
                                        }}>
                                             <Text style={css.text_perfil}>Distância (Km): {distanciaKm}</Text>
                                        </View>
                                        <View style={{
                                             width: '50%'
                                        }}>
                                             <Text style={css.text_perfil}>Tempo Estimado: {duration}</Text>
                                        </View>


                                   </View>
                                   <Text></Text>
                                   <View style={css.area__menu6}>
                                        <View style={{
                                             width: '50%'
                                        }}>
                                             <Text style={css.text_perfil}>
                                                  Custo: R${custo}

                                             </Text>
                                        </View>
                                        <View style={{
                                             width: '50%'
                                        }}>
                                             <Text style={css.text_perfil}>
                                                  Valor Sugerido: R${valorSugerido}
                                             </Text>
                                        </View>


                                   </View>
                                   <Text></Text>
                                   <View style={{
                                        flexDirection: 'row',
                                        width: '100%',
                                        paddingTop: 5,
                                        paddingBottom: 5,
                                        justifyContent: 'space-evenly',
                                        alignItems: 'center',
                                        alignSelf: 'center'

                                   }}>
                                        <Text
                                             style={css.text_perfil}>
                                             Valor Cobrado: R$ {cobradoAntigo}
                                        </Text>
                                        <TextInput
                                             placeholder="Novo valor"
                                             value={cobrado}
                                             onChangeText={setCobrado}
                                             style={css.text_perfil}
                                        />
                                   </View>
                                   <View>
                                        <Text style={css.login__msg(display)}>{erroText}</Text>
                                   </View>
                                   <Text></Text>
                                   <SafeAreaView style={{ flex: 1, marginBottom: 100 }}>
                                        <View>
                                             <TouchableOpacity style={css.menu__button_Editar} onPress={handleCalculate}>
                                                  <Text style={css.menu__buttonText}>Calcular Distânciaa</Text>
                                             </TouchableOpacity>
                                             <TouchableOpacity style={css.menu__button_Editar} onPress={() => sendForm()}>
                                                  <Text style={css.menu__buttonText}>Atualizar</Text>
                                             </TouchableOpacity>
                                        </View>
                                   </SafeAreaView>
                                   <Modal transparent visible={visible}>
                                        <SafeAreaView
                                             style={{ flex: 1 }}
                                             onTouchStart={() => resizeBox(1)}
                                        >
                                             <Animated.View style={[
                                                  css.popup,
                                                  {
                                                       opacity: scale.interpolate({
                                                            inputRange: [0, 1], outputRange: [0, 1]

                                                       })
                                                  },
                                                  {
                                                       transform: [
                                                            { scale }
                                                       ]
                                                  }]}>
                                                  <View style={css.container3}>

                                                       <Text style={css.text_perfil}>Deseja Encerrar está Viagem?</Text>
                                                       <View>
                                                            <Text style={css.login__msg(display)}>{erroText}</Text>
                                                       </View>
                                                       <TouchableOpacity style={css.menu__button_Editar} onPress={() => resizeBox(0)}>
                                                            <Text style={css.menu__buttonText}>Cancelar</Text>
                                                       </TouchableOpacity>
                                                       <TouchableOpacity style={css.menu__button_Editar} onPress={() => encerraViagem()}>
                                                            <Text style={css.menu__buttonText}>Encerrar</Text>
                                                       </TouchableOpacity>
                                                  </View>
                                             </Animated.View>
                                        </SafeAreaView>

                                   </Modal>
                              </View>
                         </ScrollView>
                    </KeyboardAvoidingView>
               </View>
          </View >
     );
}