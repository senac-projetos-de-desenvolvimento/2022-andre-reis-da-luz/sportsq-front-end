import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { Text, View, FlatList, TouchableOpacity, RefreshControl, ScrollView, SafeAreaView, Modal, Dimensions, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { css } from '../../assets/CSS/css';
import MenuSistema from '../../assets/components/MenuSistema';
import { SeparatorItem } from '../arearestrita/SeparatorItem/SeparatorItem.js'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import 'react-native-gesture-handler';



//url = "https://transport-calculator-back-dev-719001b96509.herokuapp.com"
url = `http://192.168.18.23:3000`


export default function Menu({ navigation }) {

    const [refreshing, setRefreshing] = useState(false);
    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        buscaDadosUsuario()
        renderItem
        setTimeout(() => {
            setRefreshing(false);
        }, 2000);
    }, []);



    const [dados, setDados] = useState([])
    const [user, setUser] = useState(' ')
    const [userID, setUserID] = useState(' ')
    const [oculta, setOsulta] = useState(false)
    const [idSelecionada, setIdSelecionada] = useState('')
    const [erroText, setErroText] = useState('')
    const [viagens, setViagens] = useState([])
    const [viagensEncerradas, setViagensEncerradas] = useState([])

    const [visible, setVisible] = useState(null)


    const createAlertVerificaDelete = (idSelecionada) =>
        Alert.alert(
            "Aviso:",
            "Realmente deseja Deletar esta viagem ?", [{
            text: 'Cancelar',
            style: 'cancel',
        }, {
            text: "Confirmar",
            onPress: () => sendFormDELETE(idSelecionada)
        }]
        );

        const createAlertDelete = () =>
        Alert.alert(
            "Aviso:",
            "Sucesso! Viagem Deletada!", [{
                text: "Ok",
                onPress: () => navigation.navigate('Menu')
            }]
        );

    const createAlert = () =>
        Alert.alert(
            "Aviso:", erroText
            
        );

    const createAlertVerificaEncerrar = (idSelecionada) =>
        Alert.alert(
            "Aviso:",
            "Realmente deseja Encerrar esta viagem ?", [{
                text: 'Cancelar',
                style: 'cancel',
            },{
                text: "Confirmar",
                onPress: () => encerraViagem(idSelecionada)
            }]
        );

        const createAlertEncerrar = () =>
        Alert.alert(
            "Aviso:",
            "Sucesso! Viagem Encerrada.", [{
                text: "Ok",
                onPress: () => navigation.navigate('Menu')
            }]
        );

    async function buscaDadosUsuario() {
        const value = await AsyncStorage.getItem("@asyncUsername")
        console.log(`Dados recolhidos do AsyncStorage ${value}`)
        setUser(value)

        console.log((`${url}/usuarios/valida/${value}`))
        const response = await axios.get(`${url}/usuarios/valida/${value}`)
        const dados2 = response.data
        console.log(`ID retirado do GET: ${dados2.id}`)
        const loginID = dados2.id
        setDados(response.id)

        setUserID(dados2.id)
        console.log(`ID do Usuário: ${loginID}`)
        onRefresh

        /////////////////////////////////////////////////////////////////////////////////////

        const response2 = await axios.get(`${url}/viagens/ativas/${userID}`)
        console.log(`Verificou viagens Ativas`)
        setViagens(response2.data)
        

        const response3 = await axios.get(`${url}/viagens/encerradas/${userID}`)
        console.log(`Verificou viagens Encerradas`)
        setViagensEncerradas(response3.data)
        setRefreshing(false)
        onRefresh

        return
    }

    async function encerraViagem(idSelecionada) {
        console.log(`Viagem será encerrada.`)
        console.log(`ID da Viagem: ${idSelecionada}`)

        let response = await fetch(`${url}/viagens/encerrar/${idSelecionada}`, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                emAndamento: false
            })
        });
        let json = await response.json();
        console.log(json)
        console.log(json.id)
        if (json.id === 0) {
            setErroText(json.msg)
            
            createAlert()
            setDisplay('flex');
            setTimeout(() => {
                setDisplay('none');
            }, 5000);
        } else {
            createAlertEncerrar()

        }
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////    
    const Item = ({ cliente, carga, id }) => (

        < View >
            <TouchableOpacity style={css.button__home2} onPress={() => (setVisible(true), console.log(`Id selecionada: ${id}`), setIdSelecionada(id))}>
                <View style={{
                    flexDirection: 'row',
                    height: 70,
                    padding: 10,
                    paddingRight: 10,
                }}>

                    <View>
                        <Icon name="truck" size={55} color="#555" />

                    </View>
                    <View style={{
                        flexDirection: 'column',
                        height: 60,
                        padding: 5,
                        paddingRight: 10,
                    }}>
                        <Text style={{ flex: 0.5 }} >{`Cliente: ${cliente}`}</Text>
                        <Text style={{ flex: 0.5 }}>{`Carga: ${carga}`}</Text>
                    </View>
                </View>
                <View>
                    {oculta &&
                        AlteraViagem(id = id)
                    }
                </View>
            </TouchableOpacity>
            <Modal visible={visible} transparent={true} animationType={'slide'} onRequestClose={() => setVisible(false)}>
                <SafeAreaView style={{ flex: 1, marginBottom: 100 }}>
                    <TouchableOpacity style={{ flex: 1, marginBottom: 10 }} activeOpacity={0.9} onPress={() => setVisible(false)}></TouchableOpacity>
                    <View>
                        <TouchableOpacity style={css.menu__button_Editar} onPress={() => (navigation.navigate('AlteraViagem', { selectedViagem: idSelecionada }), console.log(`ID enviada: `, idSelecionada), setVisible(false))}>
                            <Text style={css.menu__buttonText}>Editar</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={css.menu__button_Editar} onPress={() => (createAlertVerificaEncerrar(idSelecionada), console.log(`ID Encerrada: `, idSelecionada),setVisible(false))}>
                            <Text style={css.menu__buttonText}>Encerrar</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={css.menu__button_Excluir} onPress={() => (createAlertVerificaDelete(idSelecionada), setVisible(false))}>
                            <Text style={css.menu__buttonText}>Excluir</Text>
                        </TouchableOpacity>
                    </View>
                    {visible ? <View style={css.overlay} /> : null}
                </SafeAreaView>
            </Modal>
        </View >
    )
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    async function exibir(oculta) {
        if (oculta == true) {
            setOsulta(false)
            return
        }
        if (oculta == false) {
            setOsulta(true)
            return
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    const renderItem = ({ item }) => (
        <Item cliente={item.cliente} carga={item.carga} id={item.id} />

    );
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    async function AlteraViagem(id) {
        if (oculta == true) {
            setOsulta(false)
            return
        }
        if (oculta == false) {
            setOsulta(true)
            return
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    async function sendFormDELETE(id) {

        console.log(id)

        let response = await fetch(`${url}/viagens/${id}`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        });
        let json = await response.json();
        console.log(json)
        console.log(json.id)
        if (json.id === 0) {
            setErroText(json.msg)
            setDisplay('flex');
            setTimeout(() => {
                setDisplay('none');
            }, 5000);
        } else {
            buscaDadosUsuario(),
                onRefresh()
            renderItem;
            createAlertDelete()

        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////
    useEffect(() => {
        buscaDadosUsuario(),
            onRefresh()
        renderItem;
    }, []);

    return (

        <View style={[css.container, css.containerTop]}>
            <MenuSistema title='Main' navigation={navigation} />

            <Text style={css.area__title2}
                refreshControl={
                    <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                }>
                Rotas Em Andamento
            </Text>


            <FlatList
                ItemSeparatorComponent={SeparatorItem}
                data={viagens}
                keyExtractor={(item) => item.id}
                renderItem={renderItem}
                refreshControl={
                    <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                }

            />

            <Text style={css.area__title2}
                refreshControl={
                    <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                }>
                Rotas Encerradas
            </Text>


            <FlatList
                ItemSeparatorComponent={SeparatorItem}
                data={viagensEncerradas}
                keyExtractor={(item) => item.id}
                renderItem={renderItem}
                refreshControl={
                    <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                }

            />
            <View style={[css.area__menu2]}>
                <View style={[css.area__menu3]}>
                    <TouchableOpacity style={css.text_perfil2} onPress={() => navigation.navigate('Menu')}>
                        <Icon name="edit" size={20} color="#999" />
                    </TouchableOpacity>
                </View>
                <View style={[css.area__menu3]}>
                    <TouchableOpacity style={css.text_perfil2} onPress={() => navigation.navigate('CadastroViajem')}>
                        <Icon name="suitcase" size={20} color="#999" />
                    </TouchableOpacity>
                </View>
                <View style={[css.area__menu3]}>
                    <TouchableOpacity style={css.text_perfil2} onPress={() => navigation.navigate('Profile')}>
                        <Icon name="users" size={20} color="#999" />
                    </TouchableOpacity>
                </View>
            </View>

        </View >
    );
};
