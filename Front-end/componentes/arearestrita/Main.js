import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { Text, View, FlatList, TouchableOpacity, RefreshControl, ScrollView, SafeAreaView, Modal, Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { css } from '../../assets/CSS/css';
import MenuSistema from '../../assets/components/MenuSistema';
import { SeparatorItem } from '../arearestrita/SeparatorItem/SeparatorItem.js'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import 'react-native-gesture-handler';



//url = "https://transport-calculator-back-dev-719001b96509.herokuapp.com"
url = `http://192.168.18.23:3000`

export default function Main({ navigation }) {

    const [refreshing, setRefreshing] = useState(false);
    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        buscaDadosUsuario()
        buscaViagens()
        setTimeout(() => {
            setRefreshing(false);
        }, 2000);
    }, []);

    const [dados, setDados] = useState([])
    const [user, setUser] = useState(' ')
    const [userID, setUserID] = useState(' ')
    const [oculta, setOsulta] = useState(false)

    const [visible, setVisible] = useState(null)


    async function buscaDadosUsuario() {
        const value = await AsyncStorage.getItem("@asyncUsername")
        console.log(`Dados recolhidos do AsyncStorage ${value}`)
        setUser(value)

        console.log((`${url}/usuarios/valida/${value}`))
        const response = await axios.get(`${url}/usuarios/valida/${value}`)
        const dados2 = response.data
        console.log(`ID retirado do GET: ${dados2.id}`)
        const loginID = dados2.id
        setDados(response.id)

        setUserID(dados2.id)
        console.log(`ID do Usuário: ${loginID}`)
        return
    }

    /////////////////////////////////////////////////////////////////////////////////////
    const [viagens, setViagens] = useState([])
    const buscaViagens = async () => {
        const response = await axios.get(`${url}/viagens/${userID}`)
        setViagens(response.data)
        setRefreshing(false)
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////    
    const Item = ({ cliente, carga, id, valorCobrado }) => (

        < View >
            <TouchableOpacity style={css.button__home2} onPress={() => setVisible(true)}>
                <View style={{
                    flexDirection: 'row',
                    height: 70,
                    padding: 10,
                    paddingRight: 10,
                }}>

                    <View>
                        <Icon name="truck" size={55} color="#555" />
                    </View>
                    <View style={{
                        flexDirection: 'column',
                        height: 60,
                        padding: 5,
                        paddingRight: 10,
                    }}>
                        <Text style={{ flex: 0.3 }}>{`Cliente: ${cliente}`}</Text>
                        <Text style={{ flex: 0.3 }}>{`Carga: ${carga}`}</Text>
                        <Text style={{ flex: 0.3 }}>{`Valor: ${valorCobrado}`}</Text>
                    </View>
                </View>
                <View>
                    {oculta &&
                        AlteraViagem(id = id)
                    }
                </View>
            </TouchableOpacity>
            <Modal visible={visible} transparent={true} animationType={'slide'} onRequestClose={() => setVisible(false)}>
                <SafeAreaView style={{ flex: 1, marginBottom: 100 }}>
                    <TouchableOpacity style={{ flex: 1, marginBottom: 10 }} onPress={() => setVisible(false)}></TouchableOpacity>
                    <View>
                        <TouchableOpacity style={css.menu__button_Editar}> onPress={() => props.navigation.navigate('AlteraViagem')}
                            <Text style={css.menu__buttonText}>Editar</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={css.menu__button_Excluir} onPress={() => sendFormDELETE(id)}>
                            <Text style={css.menu__buttonText}>Excluir</Text>
                        </TouchableOpacity>
                    </View>
                    {visible ? <View style={css.overlay} /> : null}
                </SafeAreaView>
            </Modal>
        </View >
    )
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    async function exibir(oculta) {
        if (oculta == true) {
            setOsulta(false)
            return
        }
        if (oculta == false) {
            setOsulta(true)
            return
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    const renderItem = ({ item }) => (
        <Item cliente={item.cliente} carga={item.carga} id={item.id} valorCobrado={item.valorCobrado} />
    );
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    async function AlteraViagem(id) {
        if (oculta == true) {
            setOsulta(false)
            return
        }
        if (oculta == false) {
            setOsulta(true)
            return
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    async function sendFormDELETE(id) {

        console.log(id)

        let response = await fetch(`${url}/viagens/${id}`, {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        });
        let json = await response.json();
        if (json === 'error') {
            setDisplay('flex');
            setTimeout(() => {
                setDisplay('none');
            }, 5000);
        } else {
            buscaDadosUsuario(),
                buscaViagens()
            renderItem;

        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////
    useEffect(() => {
        buscaDadosUsuario(),
            buscaViagens()
        renderItem;
    }, []);

    return (

        <View style={[css.container, css.containerTop]}>
            <MenuSistema title='Main' navigation={navigation} />

            <Text style={css.area__title2}
                refreshControl={
                    <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                }>
                Rotas Salvas
            </Text>


            <FlatList
                ItemSeparatorComponent={SeparatorItem}
                data={viagens}
                keyExtractor={(item) => item.id}
                renderItem={renderItem}
                refreshControl={
                    <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                }

            />

        </View >


    );

};