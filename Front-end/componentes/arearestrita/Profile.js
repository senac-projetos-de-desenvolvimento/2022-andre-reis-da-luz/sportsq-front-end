import React, { useState, useEffect, useRef } from 'react';
import { Text, View, KeyboardAvoidingView, TouchableOpacity, TextInput, Button, Modal, SafeAreaView, ScrollView, Animated, Easing, RefreshControl, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { css } from '../../assets/CSS/css';
import MenuSistema from '../../assets/components/MenuSistema';
import 'react-native-gesture-handler';
import Select from 'react-select';
import { Picker } from '@react-native-picker/picker';
import axios from 'axios';

//url = `https://transport-calculator-back-dev-719001b96509.herokuapp.com`
url = `http://192.168.18.23:3000`
export default function Profile({ navigation }) {
    const [display, setDisplay] = useState('none');
    const [erroText, setErroText] = useState('')

    const [motorista, setMotorista] = useState(null)
    const [placa, setPlaca] = useState(null)
    const [marca, setMarca] = useState(null)
    const [tipo, setTipo] = useState(null)
    const [media, setMedia] = useState(null)

    const [usuarioId, setUsuarioId] = useState(' ')

    const [dados, setDados] = useState([])
    const [user, setUser] = useState(' ')
    const [userID, setUserID] = useState(' ')
    const [veiculos, setveiculos] = useState([])
    const [precoCombustivel, setPrecoCombustivel] = useState(null)

    const [disTotal, setdisTotal] = useState(' ')
    const [rendimento, setRendimento] = useState(' ')
    const [numViagens, setNumViagens] = useState(' ')
    const [kmMedio, setKmMedio] = useState(' ')
    const [tempoTotal, setTempoTotal] = useState(' ')

    const [options, setOptions] = useState([]);
    const [selectedValue, setSelectedValue] = useState('');


    const createAlertVeiculo = () =>
        Alert.alert(
            "Aviso:",
            "Veículo Cadastrado com Sucesso!"
        );

    const createAlertCombustivel = () =>
        Alert.alert(
            "Aviso:",
            "Combustível Atualizado com Sucesso!"
        );

    async function carregaDados() {
        const value = await AsyncStorage.getItem("@asyncUsername")
        console.log(`Dados recolhidos do AsyncStorage ${value} - Tela Perfil`)
        setUser(value)

        console.log((`${url}/usuarios/valida/${value}`))
        const response = await axios.get(`${url}/usuarios/valida/${value}`)
        const dados2 = response.data

        console.log(`ID retirado do GET: ${dados2.id}`)
        const loginID = dados2.id
        setDados(response.id)

        console.log(`Valor do Combustível retirado do GET: ${dados2.precoCombustivel}`)
        const loginCombustivel = dados2.precoCombustivel
        setPrecoCombustivel(loginCombustivel)

        setUserID(dados2.id)
        console.log(`ID do Usuário: ${loginID}`)


        ///////////////////////////////////////////////////////////////
        const responseGet = await axios.get(`${url}/veiculos/retornarplacas/${loginID}`)
        const identificaPlaca = responseGet.data
        setOptions(identificaPlaca)
        console.log(`Dados retirados do GET-Veículos: ${responseGet.data}`)
        console.log(`Veículos do cliente: ${identificaPlaca}`)


        ///////////////////////////////////////////////////////////////

        const responseGetGeral = await axios.get(`${url}/viagens/geral/estatisticas`)
        const identificaRecebido = responseGetGeral.data
        console.log(`Dados retirados do GET-Estatisticas Geral: ${responseGetGeral.data}`)


        return
    }

    async function sendForm() {

        console.log(`ID do Usuário: ${userID}`)

        let response = await fetch(`${url}/veiculos`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                placa: placa,
                marca: marca,
                tipo: tipo,
                media: media,
                usuario_id: userID

            })
        });
        let json = await response.json();
        console.log(json)
        console.log(json.id)
        setLogin(json)
        if (json.id === 0) {
            setErroText(json.msg)
            setDisplay('flex');
            setTimeout(() => {
                setDisplay('none');
            }, 5000);
        } else {
            createAlertVeiculo()
            setPlaca('')
            setMarca('')
            setTipo('')
            setMedia('')
            resizeBox(0)
            carregaDados()
            //navigation.navigate('Main');

        }
    }

    async function sendFormCombustivel() {

        console.log(`ID do Usuário: ${userID}`)
        console.log(`${url}/usuarios/atualizar/${userID}/${precoCombustivel}`)
        let response = await fetch(`${url}/usuarios/atualizar`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: userID,
                precoCombustivel: precoCombustivel

            })
        });
        let json = await response.json();
        console.log(json)
        console.log(json.id)
        if (json.id === 0) {
            setErroText(json.msg)
            setDisplay('flex');
            setTimeout(() => {
                setDisplay('none');
            }, 5000);
        } else {
            createAlertCombustivel()
            carregaDados()
            //navigation.navigate('Main');

        }
    }

    async function sendPlacaSelecionada() {

        console.log(`ID enviado para Estatistica: ${userID}`)
        console.log(`PLACA enviada para Estatistica: ${selectedValue}`)
        console.log(`${url}/viagens/geral/estatisticas/${userID}/${selectedValue}`)

        let responseEstatistica = await axios.get(`${url}/viagens/geral/estatisticas/${userID}/${selectedValue}`)
        console.log(`Resposta da verificação das estátisticas1: ${responseEstatistica}`)
        console.log(`Resposta da verificação das estátisticas2: ${responseEstatistica.data}`)
        let identificaEstatistica = responseEstatistica.data
        console.log(`Resposta da verificação das estátisticas3: ${identificaEstatistica}`)

        console.log(`Distancia Total: ${identificaEstatistica.distanciaTotal}`)
        setdisTotal(identificaEstatistica.distanciaTotal.toFixed(2))
        console.log(`Rendimento Bruto: ${identificaEstatistica.rendimentoBruto}`)
        setRendimento(identificaEstatistica.rendimentoBruto.toFixed(2))
        console.log(`Numedo de Viagens: ${identificaEstatistica.numeroDeViagens}`)
        setNumViagens(identificaEstatistica.numeroDeViagens)
        console.log(`KM Média Percorrida: ${identificaEstatistica.kmMedio}`)
        setKmMedio(identificaEstatistica.kmMedio.toFixed(2))
        console.log(`Tempo Total Em Viagens: ${identificaEstatistica.tempoTotal}`)
        setTempoTotal(identificaEstatistica.tempoTotal)
    }





    useEffect(() => {
        carregaDados()
    }, [])


    const [visible, setVisible] = useState(false)
    const scale = useRef(new Animated.Value(0)).current;

    function resizeBox(to) {
        to === 1 && setVisible(true);
        Animated.timing(scale, {
            toValue: to,
            useNativeDriver: true,
            duration: 200,
            easing: Easing.linear,
        }).start(() => to === 0 && setVisible(false));
    }

    return (

        <View style={[css.container, css.containerTop]}>
            <MenuSistema title='Perfil' navigation={navigation} />

            <SafeAreaView style={{
                flexDirection: 'row',
                paddingTop: 5,
                paddingBottom: 2,
            }}>
                <View style={{ width: '80%', }}></View>
                <View >
                    <TouchableOpacity style={css.menu__button_Editar} onPress={() => resizeBox(1)}>
                        <Icon name="car" size={15} color="#999" backgroundColor='#777' />
                    </TouchableOpacity>
                </View>
            </SafeAreaView>

            <KeyboardAvoidingView behavior={Platform.OS == "ios" ? "padding" : "height"} style={[css.container]}>
                <ScrollView>
                    <View >
                        <View style={css.container5}>
                            <Icon name="user-circle" size={70} color="#999" />
                        </View>
                        <View>
                            <Text style={css.login__msg(display)}>{erroText}</Text>
                        </View>
                        <View style={css.container4}>
                            {/*Teste do Select*/}
                            <View style={{
                                flexDirection: 'row',
                                paddingTop: 5,
                                paddingBottom: 5,
                                justifyContent: 'center',

                            }}>
                                <Picker
                                    style={{ width: '80%', backgroundColor: '#888' }}
                                    selectedValue={selectedValue}
                                    onValueChange={(itemValue, indexItem) =>
                                        setSelectedValue(itemValue)
                                    }>
                                    <Picker.Item
                                        label='Placas: '
                                        value=''
                                    />
                                    {options.map(option => {
                                        return <Picker.Item
                                            key={option}
                                            label={option}
                                            value={option}
                                        />
                                    }
                                    )}
                                </Picker>
                                <View style={{ width: '20%', alignItems: 'center', justifyContent: 'center', backgroundColor: '#777' }}>
                                    <TouchableOpacity onPress={() => sendPlacaSelecionada()}>
                                        <Icon name="search" size={20} color="#999" />
                                    </TouchableOpacity>
                                </View>

                            </View>
                            <Text></Text>
                            <View style={css.area__menu6}>
                                <View style={{
                                    width: '50%',
                                }}>
                                    <Text style={css.text_perfil}>
                                        {`Distancia Total: `}
                                    </Text>
                                    <Text style={css.text_perfil}>
                                        {disTotal}
                                    </Text>
                                </View>

                                <View style={{
                                    width: '50%',
                                }}>
                                    <Text style={css.text_perfil}>
                                        {`Número de Viagens: `}
                                    </Text>
                                    <Text style={css.text_perfil}>
                                        {numViagens}
                                    </Text>
                                </View>

                            </View>
                            <Text></Text>
                            <View style={css.area__menu6}>
                                <Text style={css.text_perfil}>
                                    {`Rendimento Bruto: `}
                                </Text>
                                <Text style={css.text_perfil}>
                                    R${rendimento}
                                </Text>
                            </View>
                            <Text></Text>
                            <View style={css.area__menu6}>
                                <View style={{
                                    width: '50%',
                                }}>
                                    <Text style={css.text_perfil}>
                                        {`Km médio p/ viagem: `}
                                    </Text>
                                    <Text style={css.text_perfil}>
                                    {kmMedio}
                                </Text>
                                </View>
                                <View style={{
                                    width: '50%',
                                }}>
                                    <Text style={css.text_perfil}>

                                        {`Tempo total: `}
                                    </Text>
                                    <Text style={css.text_perfil}>
                                    {tempoTotal}
                                </Text>
                                    </View>


                            </View>

                            <Text></Text>
                            <TextInput
                                placeholder={`Preço do Comb.: `}
                                value={precoCombustivel}
                                onChangeText={setPrecoCombustivel}
                                style={css.text_perfil}
                            />
                            <TouchableOpacity style={css.menu__button_Editar} onPress={() => sendFormCombustivel()}>
                                <Text style={css.menu__buttonText}>Atualizar</Text>
                            </TouchableOpacity>
                            <Text></Text>

                            <Modal transparent visible={visible}>
                                <SafeAreaView
                                    style={{ flex: 1 }}
                                    onTouchStart={() => resizeBox(1)}
                                >
                                    <Animated.View style={[
                                        css.popup,
                                        {
                                            opacity: scale.interpolate({
                                                inputRange: [0, 1], outputRange: [0, 1]

                                            })
                                        },
                                        {
                                            transform: [
                                                { scale }
                                            ]
                                        }]}>
                                        <View style={css.container3}>

                                            <TextInput
                                                placeholder="Placa:"
                                                value={placa}
                                                onChangeText={setPlaca}
                                                style={css.text_perfil}
                                            />
                                            <TextInput
                                                placeholder="Marca do Veículo:"
                                                value={marca}
                                                onChangeText={setMarca}
                                                style={css.text_perfil}
                                            />
                                            <TextInput
                                                placeholder="Tipo:"
                                                value={tipo}
                                                onChangeText={setTipo}
                                                style={css.text_perfil}
                                            />
                                            <TextInput
                                                placeholder="Média de Consumo do Veículo:"
                                                value={media}
                                                onChangeText={setMedia}
                                                style={css.text_perfil}
                                            />
                                            <Text></Text>
                                            <View>
                                                <Text style={css.login__msg(display)}>{erroText}</Text>
                                            </View>
                                            <TouchableOpacity style={css.menu__button_Editar} onPress={() => resizeBox(0)}>
                                                <Text style={css.menu__buttonText}>Cancelar</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={css.menu__button_Editar} onPress={() => sendForm()}>
                                                <Text style={css.menu__buttonText}>Cadastrar</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </Animated.View>
                                </SafeAreaView>

                            </Modal>

                        </View>

                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        </View >

    );
}

