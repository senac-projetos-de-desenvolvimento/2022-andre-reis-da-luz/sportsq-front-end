import axios from 'axios';
import React, { useState, useEffect } from 'react';
import {
     Text, View, KeyboardAvoidingView, TouchableOpacity, TextInput, ScrollView, Platform, StyleSheet, Dimensions, Button, Box, True, SafeAreaView, Pressable, Alert
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { css } from '../../assets/CSS/css';
import AsyncStorage from "@react-native-async-storage/async-storage";
import MenuSistema from '../../assets/components/MenuSistema';
import { Picker } from '@react-native-picker/picker';

import DateTimePicker from '@react-native-community/datetimepicker';


import { Autocomplete } from '@react-google-maps/api'
import calculateDistanceAndTime from '../../assets/components/CalculateDistance';

//url = "https://transport-calculator-back-dev-719001b96509.herokuapp.com"
url = "http://192.168.18.23:3000"

export default function CadastroViajem({ navigation }) {
     const [display, setDisplay] = useState('none');
     const [erroText, setErroText] = useState('')


     const [origin, setOrigin] = useState('');
     const [destination, setDestination] = useState('');
     const [distance, setDistance] = useState('');
     const [duration, setDuration] = useState('');

     const [cliente, setCliente] = useState(null);
     const [telefone, setTelefone] = useState(null);
     const [placa, setPlaca] = useState(null);
     const [carga, setCarga] = useState(null);
     const [valorSugerido, setValorSugerido] = useState(null);
     const [distanciaKm, setDistanciaKm] = useState(null);
     const [duracaoViagem, setDuracaoViagem] = useState(null);
     const [media, setMedia] = useState(null)

     const [veiculoID, setVeiculoID] = useState(null)
     const [custo, setCusto] = useState(null)
     const [cobrado, setCobrado] = useState('')


     const [precoCombustivel, setPrecoCombustivel] = useState('')
     const [dados, setDados] = useState([])
     const [user, setUser] = useState(' ')
     const [userID, setUserID] = useState(' ')

     const [options, setOptions] = useState([]);
     const [selectedValue, setSelectedValue] = useState('');

     const [date, setDate] = useState(new Date())
     const [show, setShow] = useState(false);

     const createAlertCadastro = () =>
     Alert.alert(
         "Aviso:",
         "Sucesso! Cadastro de viagem realizado.", [{
             text: "OK",
             onPress: () =>  navigation.navigate('Menu')
         }]
     );



     const onChange = (event, selectedDate) => {
          const currentDate = selectedDate || date;
          setShow(false);
          setDate(currentDate);
     };

     const showDatePicker = () => {
          setShow(true);
     };

     async function chamaDados() {

          console.log(`Data Armazenada: ${date}`)
          const value = await AsyncStorage.getItem("@asyncUsername")
          console.log(`Dados recolhidos do AsyncStorage ${value}`)
          setUser(value)

          console.log((`${url}/usuarios/valida/${value}`))
          const response = await axios.get(`${url}/usuarios/valida/${value}`)
          const dados2 = response.data

          console.log(`ID retirado do GET: ${dados2.id}`)
          const loginID = dados2.id
          setDados(loginID)

          console.log(`Valor do Combustível retirado do GET: ${dados2.precoCombustivel}`)
          const loginCombustivel = dados2.precoCombustivel
          setPrecoCombustivel(loginCombustivel)

          setUserID(dados2.id)
          console.log(`ID do Usuário: ${loginID}`)

          ///////////////////////////////////////////////////////////////
          const responseGet = await axios.get(`${url}/veiculos/retornarplacas/${loginID}`)
          const identificaPlaca = responseGet.data
          setOptions(identificaPlaca)
          console.log(`Dados retirados do GET-Veículos: ${responseGet.data}`)
          console.log(`Veículos do cliente: ${identificaPlaca}`)

     }



     const handleCalculate = async () => {
          try {
               console.log(`Placa Enviada: ${selectedValue}`)
               const response2 = await axios.get(`${url}/veiculos/${selectedValue}`)
               console.log(`Recebido da verificação de Placa: ${response2.data}`)
               console.log(`Recebido da verificação de Placa: ${response2.id}`)
               resp = response2.data
               for (i = 0; i <= (resp.length); i++) {
                    if (placa === (resp[i].placa)) {
                         setVeiculoID(resp[i].id)
                         console.log(`ID do Veículo: ${veiculoID}`)
                    }
               }

               const response3 = await axios.get(`${url}/media/${selectedValue}`)
               console.log(`Recebido pela Media: ${response3.data}`)
               setMedia(response3.data)
               console.log(`Media do Veículo recebido pelo get: ${media}`)



               const result = await calculateDistanceAndTime(origin, destination);
               setDistance(result.distance);
               setDuration(result.duration);
               console.log(`Aqui valor antes do slice ${distance}`)
               const custoViagem = Number((distance.slice(0, 3)))
               console.log(`Distancia da Viagem: ${custoViagem}`)
               setDistanciaKm(custoViagem)
               

               console.log(`Aqui valor duração antes do slice ${duration}`)
               if (duration.length > 7) {
                    const horas = Number(duration.slice(0, 2))
                    console.log(`Horas: ${horas}`)
                    const horasParaM = horas*60
                    console.log(`calc: ${horasParaM}`)
                    const minutos = Number(duration.slice(8, 10))
                    console.log(`Minutos: ${minutos}`)
                    const duracaoViagem = horasParaM + minutos
                    console.log(`Duração da Viagem(Min): ${duracaoViagem}`)
                    const duracaoViagemH = duracaoViagem/60
                    console.log(`Duração da Viagem(Hr): ${duracaoViagemH}`)
                    setDuracaoViagem(duracaoViagemH.toFixed(2))
               } else {
                    const minutos = (duration.slice(0, 2))
                    const duracaoViagem = Number(minutos)
                    console.log(`Duração da Viagem(Minutos): ${duracaoViagem}`)
                    setDuracaoViagem(duracaoViagem)
               }
               
               
               


               const distanciaNumber = Number(custoViagem).toFixed(2)
               const mediaNumber = Number(media).toFixed(2)
               const gastoLitros = (distanciaNumber / mediaNumber).toFixed(2)
               const litro = precoCombustivel
               const gasto = gastoLitros * litro


               console.log(`Media Recebida: ${mediaNumber}`)
               console.log(`Distancia Recebida: ${distanciaNumber}`)
               console.log(`Media do Veículo: ${media}`)
               console.log(`Gasto do Veículo/L: ${gastoLitros}`)
               console.log(`Valor do L: ${litro}`)
               console.log(`Custo da Viagem: ${gasto}`)
               setCusto(gasto.toFixed(2))

               if (gasto < 2) {
                    const valorFinal = ((gasto * 2) + 6).toFixed(2)
                    console.log(`Valor a ser Cobrado: ${valorFinal}`)

                    setValorSugerido(valorFinal)
               }
               if ((gasto > 2) && (gasto <= 6)) {
                    const valorFinal = ((gasto * 1.5) + 6).toFixed(2)
                    console.log(`Valor a ser Cobrado: ${valorFinal}`)

                    setValorSugerido(valorFinal)
               }

               if (gasto > 6 && gasto < 50) {
                    const valorFinal = ((gasto * 1.5) + 4).toFixed(2)
                    console.log(`Valor a ser Cobrado: ${valorFinal}`)

                    setValorSugerido(valorFinal)
               }
               if (gasto >= 50 && gasto < 75) {
                    const valorFinal = ((gasto * 2)).toFixed(2)
                    console.log(`Valor a ser Cobrado: ${valorFinal}`)

                    setValorSugerido(valorFinal)
               }
               if (gasto > 75) {
                    const valorFinal = ((gasto * 2.5)).toFixed(2)
                    console.log(`Valor a ser Cobrado: ${valorFinal}`)

                    setValorSugerido(valorFinal)
               }


          } catch (error) {
               setErroText('Erro ao realizar cálculos. Tente novamente mais tarde.')
               console.log(error);
          }
     };
     async function sendForm() {


          let response = await fetch(`${url}/viagens`, {
               method: 'POST',
               headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
               },
               body: JSON.stringify({
                    cliente: cliente,
                    motorista: user,
                    telefone: telefone,
                    placa: selectedValue,
                    carga: carga,
                    enderecoInicial: origin,
                    enderecoFinal: destination,
                    valorCobrado: cobrado,
                    custo: custo,
                    duracaoHoras: duracaoViagem,
                    usuario_id: userID,
                    distanciaKm: distanciaKm,
                    veiculo_id: veiculoID,
                    dataDaViagem: date,
                    emAndamento: true
               })
          });
          let json = await response.json();
          console.log(json)
          console.log(json.id)
          if (json.id === 0) {
               setErroText(json.msg)
               setDisplay('flex');
               setTimeout(() => {
                    setDisplay('none');
               }, 5000);
          } else {
               setOrigin('')
               setDestination('')
               setDistance('')
               setDuration('')
               setCliente('')
               setTelefone('')
               setPlaca('')
               setCarga('')
               setValorSugerido('')
               setCusto('')
               setDistanciaKm('')
               createAlertCadastro()
          }
     }

     useEffect(() => {
          chamaDados()

     }, [])


     return (

          <View style={[css.container, css.containerTop]}>

               <MenuSistema title='Nova Viagem' navigation={navigation} />
               <View style={{
                    paddingTop: 2,
                    paddingBottom: 2,
                    justifyContent: 'center'
               }}>
                    <KeyboardAvoidingView behavior={Platform.OS == "ios" ? "padding" : "height"} style={[css.container]}>
                         <ScrollView>
                              <View style={css.container4}>
                                   <View style={css.area__menu6}>
                                        <View style={{
                                             width: '50%'
                                        }}>
                                             <TextInput
                                                  placeholder="Cliente:"
                                                  value={cliente}
                                                  onChangeText={setCliente}
                                                  style={css.text_perfil}
                                             />
                                        </View>
                                        <View style={{
                                             width: '50%'
                                        }}>
                                             <TextInput
                                                  placeholder="Telefone:"
                                                  value={telefone}
                                                  onChangeText={setTelefone}
                                                  style={css.text_perfil}
                                             />
                                        </View>

                                   </View>
                                   <Text></Text>
                                   <View style={css.area__menu6}>
                                        <View style={{
                                             width: '50%'
                                        }}>
                                             <Picker
                                                  style={css.text_perfil}
                                                  selectedValue={selectedValue}
                                                  onValueChange={(itemValue, indexItem) =>
                                                       setSelectedValue(itemValue)
                                                  }>
                                                  <Picker.Item
                                                       label='Placas: '
                                                       value=''
                                                  />
                                                  {options.map(option => {
                                                       return <Picker.Item
                                                            style={css.text_perfil}
                                                            key={option}
                                                            label={option}
                                                            value={option}
                                                       />
                                                  }
                                                  )}
                                             </Picker>
                                        </View>
                                        <View style={{
                                             width: '50%'
                                        }}>
                                             <TextInput
                                                  placeholder="Carga:"
                                                  value={carga}
                                                  onChangeText={setCarga}
                                                  style={css.text_perfil}
                                             />
                                        </View>
                                   </View>
                                   <Text></Text>
                                   <View style={css.area__menu6}>

                                        <TouchableOpacity onPress={showDatePicker}>
                                             <Text style={css.text_perfil}>
                                                  Data: {date.toLocaleDateString('pt-BR')}
                                             </Text>
                                        </TouchableOpacity>
                                        {show && (
                                             <DateTimePicker
                                                  value={date}
                                                  mode="date"
                                                  display="default"
                                                  onChange={onChange}
                                             />
                                        )}

                                   </View>
                                   <Text></Text>
                                   <View style={css.area__menu6}>
                                        <View style={{
                                             width: '50%'
                                        }}>
                                             <TextInput
                                                  placeholder="Endereço Inicial:"

                                                  value={origin}
                                                  onChangeText={setOrigin}
                                                  style={css.text_perfil}
                                             />
                                        </View>
                                        <View style={{
                                             width: '50%'
                                        }}>
                                             <TextInput
                                                  placeholder="Endereço Final:"                                                  value={destination}
                                                  onChangeText={setDestination}
                                                  style={css.text_perfil}
                                             />
                                        </View>

                                   </View>
                                   <Text></Text>
                                   <View style={css.area__menu6}>
                                        <View style={{
                                             width: '50%'
                                        }}>
                                             <Text style={css.text_perfil}>Distância (Km): {distanciaKm}</Text>
                                        </View>
                                        <View style={{
                                             width: '50%'
                                        }}>
                                             <Text style={css.text_perfil}>Tempo Estimado: {duration}</Text>
                                        </View>


                                   </View>
                                   <Text></Text>
                                   <View style={css.area__menu6}>
                                        <View style={{
                                             width: '50%'
                                        }}>
                                             <Text style={css.text_perfil}>
                                                  Custo: R${custo}

                                             </Text>
                                        </View>
                                        <View style={{
                                             width: '50%'
                                        }}>
                                             <Text style={css.text_perfil}>
                                                  Valor Sugerido: R${valorSugerido}
                                             </Text>
                                        </View>


                                   </View>
                                   <Text></Text>
                                   <View style={{
                                        flexDirection: 'row',
                                        width: '100%',
                                        paddingTop: 5,
                                        paddingBottom: 1,
                                        justifyContent: 'space-evenly',
                                        alignItems: 'center',
                                        alignSelf: 'center'

                                   }}>
                                        <Text
                                             style={css.text_perfil}>
                                             Valor Cobrado: R$
                                        </Text>
                                        <TextInput
                                             value={cobrado}
                                             onChangeText={setCobrado}
                                             style={css.text_perfil}
                                        />
                                   </View>
                                   <View>
                                        <Text style={css.login__msg(display)}>{erroText}</Text>
                                   </View>
                                   <Text></Text>
                                   <SafeAreaView style={{ flex: 1, marginBottom: 100 }}>
                                        <View>
                                             <TouchableOpacity style={css.menu__button_Editar} onPress={handleCalculate}>
                                                  <Text style={css.menu__buttonText}>Calcular Distânciaa</Text>
                                             </TouchableOpacity>
                                             <TouchableOpacity style={css.menu__button_Editar} onPress={() => sendForm()}>
                                                  <Text style={css.menu__buttonText}>Cadastrar</Text>
                                             </TouchableOpacity>
                                        </View>
                                   </SafeAreaView>

                              </View>
                         </ScrollView>
                    </KeyboardAvoidingView>
               </View>

          </View >

     );
}