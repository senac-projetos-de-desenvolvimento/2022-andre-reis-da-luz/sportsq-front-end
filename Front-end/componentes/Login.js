import React, { useState, useEffect } from 'react';
import { KeyboardAvoidingView, TextInput, TouchableOpacity, Image, Text, View, Alert } from 'react-native';
import { css } from '../assets/CSS/css';
//import { allowedNodeEnvironmentFlags } from 'process';
import AsyncStorage from '@react-native-async-storage/async-storage';

//url = "https://transport-calculator-back-dev-719001b96509.herokuapp.com"
url = `http://192.168.18.23:3000`

export default function Login({ navigation }) {
    const [display, setDisplay] = useState('none');
    const [username, setUser] = useState(null);
    const [password, setPassword] = useState(null);
    const [login, setLogin] = useState([]);

    //url = "https://transport-calculator-back-dev-719001b96509.herokuapp.com"
    url = `http://192.168.18.23:3000`


   

    const createSimpleAlertS = () =>
        Alert.alert(
            "Login:",
            "Sucesso! Usuário Logado.", [{
                text: "OK",
                onPress: () =>  navigation.navigate('Menu')
            }]
        );




    async function sendForm() {
        const setStringValue = async () => {
            try {
                const asyncUsername = String(username)
                await AsyncStorage.setItem("@asyncUsername", asyncUsername)
                console.log(`Username Armazenado pelo AsyncStorage: ${asyncUsername}`)
            } catch (e) {
                console.log("Username não foi armazenado pelo AsyncStorage")
            }
        }



        let response = await fetch(`${url}/usuarios/login`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username: username,
                password: password
            })
        });



        let json = await response.json();
        console.log(json)
        console.log(json.id)
        setLogin(json)
        if (json.id === 0) {

            setDisplay('flex');
            setTimeout(() => {
                setDisplay('none');
            }, 5000);
        } else {
            setStringValue()
            createSimpleAlertS()
        }

    }

    return (
        <KeyboardAvoidingView behavior={Platform.OS == "ios" ? "padding" : "height"} style={[css.container, css.darkbg]}>
            <View style={css.login__logomarca}>
                <Image source={require('../assets/img/favicon.png')} />
            </View>

            <View>
                <Text style={css.login__msg(display)}>Usuário ou senha inválidos!</Text>
            </View>

            <View style={css.login__form}>
                <TextInput style={css.login__input} placeholder='Usuário:' onChangeText={text => setUser(text)} />
                <TextInput style={css.login__input} placeholder='Senha:' secureTextEntry={true} onChangeText={text => setPassword(text)} />

                <TouchableOpacity style={css.login__button} onPress={() => sendForm()}>
                    <Text style={css.login__buttonText}>Entrar</Text>
                </TouchableOpacity>
            </View>

        </KeyboardAvoidingView>
    );
}