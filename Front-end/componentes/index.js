import Home from './Home';
import Login from './Login';
import Menu from './arearestrita/Menu';
import Main from './arearestrita/Main';
import Profile from './arearestrita/Profile';
import CadastroViajem from './arearestrita/CadastroViajem';
import Edicao from './arearestrita/Edicao';
import AlteraViagem from './arearestrita/AlteraViagem'

export {Home,Login,Menu, Main,Profile,CadastroViajem,Edicao,AlteraViagem };