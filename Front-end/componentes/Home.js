import React, { useEffect } from 'react';
import { Text, View, Image, TouchableOpacity } from 'react-native';
import { css } from '../assets/CSS/css';
import Icon from 'react-native-vector-icons/FontAwesome';
import AsyncStorage from '@react-native-async-storage/async-storage';




export default function Home({ navigation }) {
    const clearAll = async () => {
        try {
            await AsyncStorage.clear()
            console.log("AsyncStorage limpo.")
        } catch (e) {
            console.log("AsyncStorage Comprometido.")
        }
    }
    useEffect(() => {
        clearAll();
    }, []);
    return (
        <View style={css.container2}>
            <TouchableOpacity style={css.button__home} onPress={() => navigation.navigate('Login')}>
                <Icon name="users" size={50} color="#999" />
                <Text>Login</Text>
            </TouchableOpacity>
            <TouchableOpacity style={css.button__home} onPress={() => navigation.navigate('Create')}>
                <Icon name="plus-square" size={50} color="#999" />
                <Text>Cadastrar</Text>
            </TouchableOpacity>
        </View>
    );
}