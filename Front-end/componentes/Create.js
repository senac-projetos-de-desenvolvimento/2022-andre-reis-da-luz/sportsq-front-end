import React, {useState,useEffect} from 'react';
import { KeyboardAvoidingView, TextInput, TouchableOpacity, Image, Text, View, Alert} from 'react-native';
import {css} from '../assets/CSS/css';
//import { allowedNodeEnvironmentFlags } from 'process';
import AsyncStorage from '@react-native-async-storage/async-storage';

//url = `https://transport-calculator-back-dev-719001b96509.herokuapp.com`
url = `http://192.168.18.23:3000`
export default function Create({navigation})
{
    const [display, setDisplay]=useState('none');
    const [username, setUser]=useState(null);
    const [valorCombustivel,setValorCombustivel] = useState('none')
    const [password, setPassword]=useState(null);
    const [login, setLogin]=useState(null);

    const createSimpleAlertS = () =>
        Alert.alert(
            "Cadastro:",
            "Sucesso! Usuário Cadastrado.", [{
                text: "OK",
                onPress: () =>  navigation.navigate('Home')
            }]
        );

    async function sendForm()
    {
        let response=await fetch(`${url}/usuarios/cadastrar`,{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username: username,
                password: password,
                precoCombustivel: valorCombustivel
            })
        });
        let json = await response.json();       
        if(json === 'error'){
            setDisplay('flex');
            setTimeout(()=>{
                setDisplay('none');
            },5000);
        }else{         
            createSimpleAlertS()
        }
        
    }

    return(
        <KeyboardAvoidingView behavior={Platform.OS == "ios" ? "padding" : "height"} style={[css.container, css.darkbg]}>
            <View style={css.login__logomarca}>
                <Image source={require('../assets/img/favicon.png')} />
            </View>

            <View>
                <Text style={css.login__msg(display)}>Usuário ou senha inválidos!</Text>
            </View>

            <View style={css.login__form}>
                <TextInput style={css.login__input} placeholder='Usuário:'onChangeText={text=>setUser(text)}/>
                <TextInput style={css.login__input} placeholder='Senha:' secureTextEntry={true} onChangeText={text=>setPassword(text)}/>
                <TextInput style={css.login__input} placeholder='Combustível em sua Cidade:'onChangeText={text=>setValorCombustivel(text)}/>
                <TouchableOpacity style={css.login__button} onPress={()=>sendForm()}>
                    <Text style={css.login__buttonText}>Cadastrar</Text>
                </TouchableOpacity>
            </View>
        </KeyboardAvoidingView>
    );
}