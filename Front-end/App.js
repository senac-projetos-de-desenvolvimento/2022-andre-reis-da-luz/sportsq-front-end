import React, {useState,useEffect} from 'react';
import { Text, View, Button, Alert } from 'react-native';
import {css} from './assets/CSS/css';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import AsyncStorage from '@react-native-async-storage/async-storage';
import MapView from 'react-native-maps';

import Home from './componentes/Home';
import Login from './componentes/Login';
import Create from './componentes/Create';
import Menu from './componentes/arearestrita/Menu';
import CadastroViajem from './componentes/arearestrita/CadastroViajem';
import Profile from './componentes/arearestrita/Profile';
import AlteraViagem from './componentes/arearestrita/AlteraViagem';

export default function App() {
  const Stack = createStackNavigator();

  return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
                  name="Home"
                  component={Home}
                  options={{
                  title:"Transport Calculator",
                  headerStyle:{backgroundColor:"#888"},
                  headerTintColor:'#333',
                  headerTitleStyle:{fontWeight:'bold', alignSelf:'center'}
              }}
          />
          <Stack.Screen name="Login" options={{headerShown:false}} component={Login} />
          <Stack.Screen name="Create" options={{headerShown:false}} component={Create} />
          {<Stack.Screen name="Menu" options={{headerShown:false}} component={Menu}></Stack.Screen>}
          {<Stack.Screen name="CadastroViajem" options={{headerShown:false}} component={CadastroViajem}></Stack.Screen>}
          {<Stack.Screen name="Profile" options={{headerShown:false}} component={Profile}></Stack.Screen>}
          {<Stack.Screen name="AlteraViagem" options={{headerShown:false}} component={AlteraViagem}></Stack.Screen>}
        </Stack.Navigator>
      </NavigationContainer>
  );
}
